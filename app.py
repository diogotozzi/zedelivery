from bson.json_util import dumps
from bson.objectid import ObjectId
from bson.son import SON
from flask import Flask, abort, request, Response
from pymongo import MongoClient
import json
import pymongo

app = Flask(__name__)

client = MongoClient(host="mongodb", port=27017)
db = client.zedelivery

db.pdvs.create_index([("address", pymongo.GEOSPHERE)])
db.pdvs.create_index([("coverageArea", pymongo.GEOSPHERE)])
db.pdvs.create_index([("id", pymongo.ASCENDING)], unique=True)
db.pdvs.create_index([("document", pymongo.ASCENDING)], unique=True)

@app.route("/pdvs", methods=["GET"])
def pdvs():

    longitude = float(request.args.get("longitude", 0))
    latitude = float(request.args.get("latitude", 0))
    
    if (longitude == 0 or latitude == 0):
        abort(400)
        abort(Response("Lat and long fields are mandatory"))

    query = {'coverageArea': {'$geoIntersects': SON([('$geometry', SON([('type', 'Point'), ('coordinates', [longitude, latitude])])) ])}}
    
    try:
        pdvs = db.pdvs.find(query, {"_id":0})
    except Exception as err:
        return "error: %s" % err
    
    return dumps(pdvs)

@app.route("/pdv", methods=["GET"])
def get_pdv():

    pdv_id = request.args.get("id", 0)
    
    if (pdv_id == 0):
        abort(Response("'id' field is required"))
    
    query = {"id": pdv_id}
    
    try:
        pdv = db.pdvs.find(query, {"_id":0})
    except Exception as err:
        return "error: %s" % err
        
    return dumps(pdv)
    
@app.route("/pdv", methods=["POST"])
def post_pdv():

    pdv = json.loads(request.data)
    
    if "id" not in pdv:
        abort(400)
        abort(Response("'id' field is required"))
    
    if "coverageArea" not in pdv:
        abort(400)
        abort(Response("'coverageArea' field is required"))
    
    if "address" not in pdv:
        abort(400)
        abort(Response("'address' field is required"))
        
    try:
        pdv["id"] = str(pdv["id"])
        db.pdvs.insert_one(pdv)
    except Exception as err:
        return "error: %s" % err

    return str(pdv["id"])

if __name__ == "__main__":
    app.run(host="0.0.0.0")
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
