## About

Thanks to consider me for the job 😃.

This project uses Docker, Python 3, MongoDB 4 and Flask Framework.


### How to run the stack

Just run the command below in your terminal

```
docker-compose up
```

PS: The database will have all PDVs provided in GeoJSON test.

### Insert a new PDV

Make a POST request to ```http://127.0.0.1:5000/pdv``` with the following body

```
{
    "id": 99, 
    "tradingName": "Adega do Diogo Tozzi",
    "ownerName": "Diogo Tozzi",
    "document": "000.000.000-00",
    "coverageArea": { 
        "type": "MultiPolygon", 
        "coordinates": [
            [[[30, 20], [45, 40], [10, 40], [30, 20]]], 
            [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
        ]
    },
    "address": { 
        "type": "Point",
        "coordinates": [10, 40]
    }
}
```

### Get PDV by ID

Make a GET request to ```http://127.0.0.1:5000/pdv?id=99``` with the ID parameter

### Search for PDVs

Just make a GET request to ```http://127.0.0.1:5000/pdvs?longitude=10&latitude=40```
with the longitude and latitude parameters

### Tests

Unfortunately I did not have time to code _functional tests_.

As this project does not produces any new class, _unit tests_ are a little bit out of context.
